/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class Usuario {
    private String nome;
    private ArrayList<Tarefa> tarefasUser;
    private String idUser;

    public Usuario(String nome, ArrayList<Tarefa> tarefasUser, String idUser) {
        this.nome = nome;
        this.tarefasUser = tarefasUser;
        this.tarefasUser = new ArrayList<>();
        this.idUser = idUser;
    }

    public Usuario() {
    }
    

    public Usuario(String nome, String idUser) {
        this.nome = nome;
        this.idUser = idUser;
    }
    
    public String getNome() {
        return nome;
    }

    public ArrayList<Tarefa> getTarefasUser() {
        return tarefasUser;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTarefasUser(ArrayList<Tarefa> tarefasUser) {
        this.tarefasUser = tarefasUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    
    
}