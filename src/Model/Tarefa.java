/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;
import java.util.Date;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;


public class Tarefa {
    private String descricao;
    private String idTarefa;
    private Date data;
    private String assignedUserId;

    public Tarefa(String descricao, String idTarefa, Date data, String assignedUserId) {
        this.descricao = descricao;
        this.idTarefa = idTarefa;
        this.data = data;
        this.assignedUserId = assignedUserId;
    }

    public Tarefa() {
    }

    public String getAssignedUserId() {
        return assignedUserId;
    }
    

    public String getDescricao() {
        return descricao;
    }

    public String getIdTarefa() {
        return idTarefa;
    }

    public Date getData() {
        return data;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setIdTarefa(String idTarefa) {
        this.idTarefa = idTarefa;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public void setAssignedUserId(String assignedUserId) {
        this.assignedUserId = assignedUserId;
    }
    
    
}
