/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Model.Tarefa;
import Model.Usuario;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author andre
 */
public class ControleTarefa {
    private ArrayList<Tarefa> listaTarefasTotal;
    
    ControleUsuario umControleUsuario = new ControleUsuario();

    public ControleTarefa(ArrayList<Tarefa> listaTarefasTotal) {
        this.listaTarefasTotal = listaTarefasTotal;
        this.listaTarefasTotal = new ArrayList<>();
    }

    public ArrayList<Tarefa> getListaTarefasTotal() {
        return listaTarefasTotal;
    }

    public void setListaTarefasTotal(ArrayList<Tarefa> listaTarefasTotal) {
        this.listaTarefasTotal = listaTarefasTotal;
    }

    public ControleTarefa() {
    }
    
    public String addTask(Tarefa umaTarefa){
        String aviso1 = "Tarefa adicionada à lista total!";
        listaTarefasTotal.add(umaTarefa);
        return aviso1;
    }
    
    public Tarefa searchTask(String idTarefa){
        for (Tarefa umaTarefa : listaTarefasTotal) {
            if(umaTarefa.getIdTarefa().equalsIgnoreCase(idTarefa)){
                return umaTarefa;
            }
        } return null;
    }
    
    public Tarefa searchTaskToUser(String idUser, String idTarefa){
        
        Usuario pesquisa = umControleUsuario.searchUserId(idUser);
        ArrayList<Tarefa> taskUser = pesquisa.getTarefasUser();
        
        for(Tarefa umaTarefa : taskUser){
            if(umaTarefa.getIdTarefa().equalsIgnoreCase(idTarefa)){
                return umaTarefa;
            }
        }return null;
        
    }
    
    public String delTask(Tarefa umaTarefa, String idUser){
        String mensagem1 = "Tarefa Deletada!";
        listaTarefasTotal.remove(umaTarefa);
        
        Usuario primitivo;
        Usuario pesquisa = umControleUsuario.searchUserId(idUser);
        primitivo = pesquisa;
        ArrayList<Tarefa> taskUser = pesquisa.getTarefasUser();
        taskUser.remove(umaTarefa);
        pesquisa.setTarefasUser(taskUser);
        umControleUsuario.delUser(primitivo);
        umControleUsuario.addUser(pesquisa);
        
        return mensagem1;
    }
    
    public String addTaskToUser(Tarefa umaTarefa, String idUser){
        String aviso2 = "Tarefa adicionada ao usuário!";
        
        addTask(umaTarefa);
        
        Usuario primitivo;
        Usuario pesquisa = umControleUsuario.searchUserId(idUser);
        primitivo = pesquisa;
        ArrayList<Tarefa> taskUser = pesquisa.getTarefasUser();
        taskUser.add(umaTarefa);
        pesquisa.setTarefasUser(taskUser);
        umControleUsuario.delUser(primitivo);
        umControleUsuario.addUser(pesquisa);
        
        
        return aviso2;
    }
    
    public String editTaskDescription(String alterar, String idTarefa){
        String warning1 = "Tarefa Alterada";
        
        searchTask(idTarefa).setDescricao(alterar);
        
        return warning1;
    }
    
    public String editTaskAssignedUser(String alterar, String idTarefa){
        String warning1 = "Tarefa Alterada";
        
        searchTask(idTarefa).setAssignedUserId(alterar);
        
        return warning1;
    }
    
    public String editTaskDate(String alterar, String idTarefa) throws ParseException{
        String warning1 = "Tarefa Alterada";
        
        SimpleDateFormat sdt = new SimpleDateFormat("dd/MM/yyyy");
        Date data = sdt.parse(alterar);
        searchTask(idTarefa).setData(data);
        
        return warning1;
    }
    
    public String editTaskDescriptionToUser(String idTarefa, String idUser, String alterar){
        String message = "Tarefa Alterada!";
        
        editTaskDescription(alterar, idTarefa);
        
        Usuario primitivo;
        Usuario pesquisa = umControleUsuario.searchUserId(idUser);
        primitivo = pesquisa;
        
        Tarefa primitiva;
        Tarefa umaTarefa = searchTaskToUser(idUser, idTarefa);
        primitiva = umaTarefa;
        umaTarefa.setDescricao(alterar);
        
        umControleUsuario.delUser(primitivo);
        umControleUsuario.addUser(pesquisa);
        
        return message;
    }
    
    public String editTaskAssignedUserToUser(String idTarefa, String idUser, String alterar){
        String message = "Tarefa Alterada!";
        
        editTaskAssignedUser(alterar, idTarefa);
        
        Usuario primitivo;
        Usuario pesquisa = umControleUsuario.searchUserId(idUser);
        primitivo = pesquisa;
        
        Tarefa primitiva;
        Tarefa umaTarefa = searchTaskToUser(idUser, idTarefa);
        primitiva = umaTarefa;
        umaTarefa.setAssignedUserId(alterar);
        
        umControleUsuario.delUser(primitivo);
        umControleUsuario.addUser(pesquisa);
        
        return message;
    }
    
    public String editTaskDateToUser(String idTarefa, String idUser, String alterar) throws ParseException{
        String message = "Tarefa Alterada!";
        
        editTaskDate(alterar, idTarefa);
        
        Usuario primitivo;
        Usuario pesquisa = umControleUsuario.searchUserId(idUser);
        primitivo = pesquisa;
        
        Tarefa primitiva;
        Tarefa umaTarefa = searchTaskToUser(idUser, idTarefa);
        primitiva = umaTarefa;
        SimpleDateFormat sdt = new SimpleDateFormat("dd/MM/yyyy");
        Date data = sdt.parse(alterar);
        umaTarefa.setData(data);
        
        umControleUsuario.delUser(primitivo);
        umControleUsuario.addUser(pesquisa);
        
        return message;
    }   
}
