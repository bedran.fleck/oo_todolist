/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Model.Usuario;
import java.util.ArrayList;

/**
 *
 * @author andre
 */
public class ControleUsuario {
    private ArrayList<Usuario> listaUsuarios;

    
    public ControleUsuario(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = new ArrayList<>();
        this.listaUsuarios = listaUsuarios;
    }

    public ControleUsuario() {
    }
    
    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public String addUser(Usuario umUsuario){
       String mensagem = "Usuario Adicionado!";
       listaUsuarios.add(umUsuario);
       return mensagem;
    }
    
    public String delUser(Usuario umUsuario){
        String mensagem1 = "Usuario Deletado!";
        listaUsuarios.remove(umUsuario);
        return mensagem1;
    }
    
    public Usuario searchUserId(String idUser){
        for (Usuario umUsuario : listaUsuarios) {
            if (umUsuario.getIdUser().equalsIgnoreCase(idUser)) {
                return umUsuario;
            }
        }
        return null;
    }
    
    public Usuario searchUserName(String nome){
        for (Usuario umUsuario : listaUsuarios) {
            if (umUsuario.getNome().equalsIgnoreCase(nome)) {
                return umUsuario;
            }
        }
        return null;
    }


}
